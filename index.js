const webdriver = require('selenium-webdriver');
require('chromedriver');
const chrome = require('selenium-webdriver/chrome');

const express = require('express');
const path = require('path');
const app = express();
const port = 3000;

const ReqQueue = require('express-request-queue');
const Queue = new ReqQueue();

const { default: PQueue } = require('p-queue');
const CreatePolicyQueue = new PQueue({ concurrency: 1 });
const RegLKQueue = new PQueue({ concurrency: 1 });
const RegMaxLKQueue = new PQueue({ concurrency: 1 });
const GetLKQueue = new PQueue({ concurrency: 1 });
const ShowRegLKQueue = new PQueue({ concurrency: 1 });
const AuthNewPolicy = new PQueue({ concurrency: 1 });

const solver = require('2captcha');

const database = require('quick.db');

const bodyParser = require('body-parser');

const { By, until, Key } = webdriver;

const regURL = 'https://lk.sogaz.ru/registration/';
const authURL = 'https://lk.sogaz.ru/auth/';
const rndEmailURL = 'https://dropmail.me/ru/';
let confirmRegURL = '';

function createBrowser (name, useHeadless = false) {
  let options = new chrome.Options();
  if (useHeadless) {
    options.addArguments('--headless');
  }
  options.addArguments('--ignore-certificate-errors');
  options.addArguments('--window-position=490,10');

  console.log('Browser created');
  return new webdriver.Builder().forBrowser(name).setChromeOptions(options).build();
}

function setValue (name, value, driver) {
  driver.findElement(By.name(name)).sendKeys(value);
}

async function clickValue (driver, name) {
  await driver.findElement(By.name(name)).click();
}

const emailFieldName = 'REGISTER[LOGIN]';
const passwordFieldName = 'REGISTER[PASSWORD]';
const confirmPasswordFieldName = 'REGISTER[CONFIRM_PASSWORD]';
const surnameFieldName = 'REGISTER[LAST_NAME]';
const nameFieldName = 'REGISTER[NAME]';
const patronymicFieldName = 'REGISTER[SECOND_NAME]';
const noPatronymicFieldName = 'custom-check custom-check_type_checkbox';
const birthdayFieldName = 'REGISTER[PERSONAL_BIRTHDAY]';
const documentFieldName = 'UF_IDENTITY_DOCUMENT[DOCUMENT_TYPE]';
const documentSeriesFieldName = 'UF_IDENTITY_DOCUMENT[SERIES]';
const documentNumberFieldName = 'UF_IDENTITY_DOCUMENT[NUMBER]';
const documentDateOfIssueFieldName = 'UF_IDENTITY_DOCUMENT[DATE_OF_ISSUE]';
const documentUnitCodeFieldName = 'UF_IDENTITY_DOCUMENT[UNIT_CODE]';
const documentIssueByFieldName = 'UF_IDENTITY_DOCUMENT[ISSUED_BY]';
const phoneFieldName = 'REGISTER[PERSONAL_PHONE]';
const innFieldName = 'REGISTER[LEGAL_ENTITY_INN]';
const regAgreeCheckboxName = 'REGISTER[AGREE]';
const submitButtonName = 'register_submit_button';

let accountForAuth = '';
let userEmail = '';
let userPassword = '123456';
let userConfirmPassword = '123456';
let userSurname = 'Иванов';
let userFirstName = 'Иван';
let userSecondName = 'Иванович';
let userBirthday = '15.07.1999';
let userDocSeries = '1234';
let userDocNumber = '123456';
let userDocDateOfIssue = '27.07.2016';
let userDocUnitCode = '123456';
let userDocIssueBy = 'ОВД Калачинского района Омской области';
let userPhone = '9509509191';
let userINN = '823498243709924';

const accDB = new database.table('Accounts');
const defaultACCsCount = 25;
const minACCsInDB = 5;
let accountsInDB = 0;
// ClearAccounts();
AccountCounter();

function SaveLoginInDB (login, accIndex = 1) {
  let acc = accDB.get(`acc${accIndex}`);
  // console.log(`acc${accIndex}: `, acc);
  if (acc !== '') {
    accIndex++;
    SaveLoginInDB(login, accIndex);
  } else {
    // console.log('acc for saving: ', acc);
    accDB.set(`acc${accIndex}`, login);
  }
}

/**
 * @return {string}
 */
async function GetLoginFromDB (request, response) {
  let accIndex = 1;
  for (accIndex; accIndex < 26; accIndex++) {
    let acc = await accDB.get(`acc${accIndex}`);
    if (acc !== '') {
      accountForAuth = await acc;
      await accDB.set(`acc${accIndex}`, '');
      await response.json(accountForAuth);
      return;
    } else {
      if (accIndex === 25 && acc === '' || accIndex === 25 && acc === null) {
        return await response.status(404).send('Account Login not found, may be DB is empty');
      }
    }
  }
}

function AccountCounter (accIndex = 1) {
  for (let i = 1; i < 26; i++) {
    if (i === 25 && accountsInDB <= minACCsInDB || i === 25 && accountsInDB === 0) {
      // SaveLoginInDB('ultqalji@supere.ml');
      // SaveLoginInDB('wadtlwh@zeroe.ml');
      // SaveLoginInDB('zticqiopc@emltmp.com');
      // SaveLoginInDB('afaxumob@emlpro.com');
      // SaveLoginInDB('exntqiopc@emltmp.com');
      // SaveLoginInDB('xqlbkvf@firste.ml');
      // SaveLoginInDB('xbixibfl@10mail.org');
      // SaveLoginInDB('kkdvgqrnb@emlhub.com');
      // SaveLoginInDB('vvklyti@zeroe.ml');
      // SaveLoginInDB('vkchvlg@laste.ml');
      // SaveLoginInDB('ueuvynki@supere.ml');
      // SaveLoginInDB('owzimjnob@emlpro.com');
      // SaveLoginInDB('fcppwlg@laste.ml');
      // SaveLoginInDB('pafyqwopc@emltmp.com');
      return AccountRegistration();
    }
    let acc = accDB.get(`acc${accIndex}`);
    if (acc !== '') {
      accIndex++;
      accountsInDB++;
    } else {
      accIndex++;
    }
  }
}

console.log('Accounts In DB: ', accountsInDB);

function ShowRegisteredAccounts (request, response, accIndex = 1) {
  let accArr = [];
  for (let i = 1; i < 26; i++) {
    console.log('i + ', i, ' array = ', accArr);
    if (i === 25 && accArr.length === 0) {
      return response.status(404).send('Accounts count in DB = 0, registration new accounts in progress');
    }
    let acc = accDB.get(`acc${accIndex}`);
    if (acc !== '') {
      accArr.push(accDB.get(`acc${accIndex}`));
      console.log(`Account for push - acc${accIndex}: `, acc);
      accIndex++;
    } else {
      accIndex++;
    }
  }

  return accArr;
}

function ShowAllAccounts () {
  for (let i = 1; i < 26; i++) {
    console.log(`acc${i}: `, accDB.get(`acc${i}`));
  }
}

function ClearAccounts (since = 1, to = 26) {
  for (let i = since; i < to; i++) {
    accDB.set(`acc${i}`, '');
    console.log(`acc${i}: `, 'has been cleared');
  }
}

function CheckLocatedElement (driver, by) {
  return webdriver.until.elementsLocated(by);
}

async function GetEmail (driver) {
  await driver.get(rndEmailURL);
  await driver.wait(webdriver.until.elementLocated(By.xpath('//*[@class="email"]')), 10000).then(element => {
    return driver.wait(webdriver.until.elementIsEnabled(element), 10000).then(async () => {
      await console.log('ELEMENT IS ENABLED');
      userEmail = await driver.findElement(By.xpath('//*[@class="email"]')).getText();
      console.log('User Email: ', userEmail);
    });
  });
}

async function GoToRegistration () {
  const driver = createBrowser('chrome');
  await driver.get(regURL);
  await setValue(emailFieldName, userEmail, driver);
  await setValue(passwordFieldName, userPassword, driver);
  await setValue(confirmPasswordFieldName, userConfirmPassword, driver);
  await setValue(surnameFieldName, userSurname, driver);
  await setValue(nameFieldName, userFirstName, driver);
  await setValue(patronymicFieldName, userSecondName, driver);
  await setValue(birthdayFieldName, userBirthday, driver);
  // await clickValue(driver, noPatronymicFieldName);
  await driver.findElement(By.xpath('/html/body/div[1]/div[2]/div/div/form/div[8]/div[2]/div/div[1]/div/select/option[2]')).click();
  await setValue(documentSeriesFieldName, userDocSeries, driver);
  await setValue(documentNumberFieldName, userDocNumber, driver);
  await setValue(documentDateOfIssueFieldName, userDocDateOfIssue, driver);
  await setValue(documentUnitCodeFieldName, userDocUnitCode, driver);
  await setValue(documentIssueByFieldName, userDocIssueBy, driver);
  await setValue(phoneFieldName, userPhone, driver);
  await clickValue(driver, regAgreeCheckboxName);
  await clickValue(driver, submitButtonName);
  await driver.wait(async function () {
    let currURL = await driver.getCurrentUrl();
    let num = 0;
    do {
      currURL = await driver.getCurrentUrl();
      if (currURL === 'https://lk.sogaz.ru/registration/success/') {
        return num = 1;
      }
    } while (num < 1);
  }, 5000);
  await driver.quit();
}

async function GoToEmail (driver, quitBrowser = true) {
  await driver.wait(CheckLocatedElement(driver, By.xpath('/html/body/div[2]/div[6]/ul/li/div[1]/div[1]/pre/a'), 5000));
  confirmRegURL = await driver.findElement(By.xpath('/html/body/div[2]/div[6]/ul/li/div[1]/div[1]/pre/a')).getAttribute('href');
  await driver.get(confirmRegURL);
  await driver.wait(driver.get(confirmRegURL), 4000);
  await console.log('LOGIN: ', userEmail, ' PASSWORD: ', userPassword);
  if (quitBrowser) {
    await driver.quit();
  }
}

app.use(express.static('public'));
// app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.route('/auth/new-policy').post(async function (request, response) {
  console.log('Authorization');
  if (request.body !== undefined) {
    await AuthNewPolicy.add(async () => {
      let driver = await createBrowser('chrome');
      await Authorization(driver, request.body).then(async function () {
        await PolicyCalculation(driver, request.body).then(async function () {

        });
      });
      response.send('Authorization is done');
    });
    return;
  }

  return response.status(404).send('Authorization is failed');
});

let cutCity = 'NOTHING';
let cutRegion = 'NOTHING';

const AuthLoginName = 'USER_LOGIN';
const AuthPasswordName = 'USER_PASSWORD';
const AuthButtonName = 'Login';
const GoToCalculationPageXpath = '/html/body/div[1]/div[2]/div[2]/div[2]/div[1]/a[2]';
const LocationCityFieldName = 'locationCity';
const LocationCityFieldXpath = `//*[contains(@name,"locationCity")][contains(@placeholder,'название города')]`;
const LocationCityFieldCSS = 'document.querySelector("#tab_section_calc > div.eosago-calculator.clearfix.eosago-calculator_edit > div.b-col-l.b-col-l_calc > form > div.b-cnt-box > div > div > input.eosago-kladr-city-autocomplete")';
const CategoryAXpath = '//*[@id="tab_section_calc"]/div[6]/div[1]/form/div[2]/div[1]/div[1]/div[2]/div/div/select/option[2]';
const CategoryBBEXpath = '//*[@id="tab_section_calc"]/div[6]/div[1]/form/div[2]/div[1]/div[1]/div[2]/div/div/select/option[3]';
const CategoryCCEXpath = '//*[@id="tab_section_calc"]/div[6]/div[1]/form/div[2]/div[1]/div[1]/div[2]/div/div/select/option[4]';
const CategoryDDEXpath = '//*[@id="tab_section_calc"]/div[6]/div[1]/form/div[2]/div[1]/div[1]/div[2]/div/div/select/option[5]';
const TSPowerFieldName = 'tsPower';
const TSReleaseYearFieldName = 'tsYear';
const TSMaxMassFieldName = 'tsMaxMass';
const TSUnladenMassFieldName = 'tsUnladenMass';
const TSPassagerCountFieldName = 'tsPassagerCount';
const TSUsingTargetFieldName = 'tsUse';
const TSUsingTargetPersonalFieldXpath = '//*[@id="calculatorTsUse"]/option[2]';
const ChangeMaleXpath = '//*[@id="tab_section_calc"]/div[7]/div[1]/form/div[2]/div[2]/div[5]/div/label[1]';
const ChangeFemaleXpath = '//*[@id="tab_section_calc"]/div[7]/div[1]/form/div[2]/div[2]/div[5]/div/label[2]';
const ChangeLicenseCategoryAXpath = '//*[@id="tab_section_calc"]/div[7]/div[1]/form/div[2]/div[2]/div[6]/div/label[1]';
const ChangeLicenseCategoryBXpath = '//*[@id="tab_section_calc"]/div[7]/div[1]/form/div[2]/div[2]/div[6]/div/label[2]';
const ChangeLicenseCategoryCXpath = '//*[@id="tab_section_calc"]/div[7]/div[1]/form/div[2]/div[2]/div[6]/div/label[3]';
const ChangeLicenseCategoryDXpath = '//*[@id="tab_section_calc"]/div[7]/div[1]/form/div[2]/div[2]/div[6]/div/label[4]';

const TSUsingPeriod3MonthFieldXpath = '//*[@id="calculatorInsurancePeriodRussia"]/option[2]';
const TSUsingPeriod4MonthFieldXpath = '//*[@id="calculatorInsurancePeriodRussia"]/option[3]';
const TSUsingPeriod5MonthFieldXpath = '//*[@id="calculatorInsurancePeriodRussia"]/option[4]';
const TSUsingPeriod6MonthFieldXpath = '//*[@id="calculatorInsurancePeriodRussia"]/option[5]';
const TSUsingPeriod7MonthFieldXpath = '//*[@id="calculatorInsurancePeriodRussia"]/option[6]';
const TSUsingPeriod8MonthFieldXpath = '//*[@id="calculatorInsurancePeriodRussia"]/option[7]';
const TSUsingPeriod9MonthFieldXpath = '//*[@id="calculatorInsurancePeriodRussia"]/option[8]';
const TSUsingPeriod10MonthFieldXpath = '//*[@id="calculatorInsurancePeriodRussia"]/option[9]';

const TSGoingToRegPlaceXpath = '//*[@id="tab_section_calc"]/div[6]/div[1]/form/div[2]/div[1]/div[2]/div/div[10]/label[1]';
const TSNotGoingToRegPlaceXpath = '//*[@id="tab_section_calc"]/div[6]/div[1]/form/div[2]/div[1]/div[2]/div/div[10]/label[2]';
const TSUsingTrailerXpath = '//*[@id="tab_section_calc"]/div[6]/div[1]/form/div[2]/div[1]/div[2]/div/div[12]/label[1]';
const TSNotUsingTrailerXpath = '//*[@id="tab_section_calc"]/div[6]/div[1]/form/div[2]/div[1]/div[2]/div/div[12]/label[2]';

const DriverCount1Xpath = '//*[@id="tab_section_calc"]/div[6]/div[1]/form/div[2]/div[3]/div/div/span/label[1]';
const DriverCount2Xpath = '//*[@id="tab_section_calc"]/div[6]/div[1]/form/div[2]/div[3]/div/div/span/label[2]';
const DriverCount3Xpath = '//*[@id="tab_section_calc"]/div[6]/div[1]/form/div[2]/div[3]/div/div/span/label[3]';
const DriverCount4Xpath = '//*[@id="tab_section_calc"]/div[6]/div[1]/form/div[2]/div[3]/div/div/span/label[4]';
const DriverCount5Xpath = '//*[@id="tab_section_calc"]/div[6]/div[1]/form/div[2]/div[3]/div/div/span/label[5]';
const DriverCountIsInfinityXpath = '//*[@id="tab_section_calc"]/div[6]/div[1]/form/div[2]/div[3]/div/div/span/label[6]';

const StartInsuranceDateFieldXpath = '/html/body/div[2]/div[1]/div/div[4]/div[6]/div[1]/form/div[2]/div[2]/div/div/div/input';

let CaptchaText = '';
const Captcha1ImageXpath = '//*[@id="tab_section_calc"]/div[6]/div[1]/form/div[2]/div[4]/div/div/div[1]/img';
const Captcha1FieldXpath = '/html/body/div[2]/div[1]/div/div[4]/div[6]/div[1]/form/div[2]/div[4]/div/div/div[2]/input';
const Captcha2ImageXpath = '//*[@id="tab_section_calc"]/div[7]/div[1]/form/div[7]/div/div/div[1]/img';
const Captcha2FieldXpath = '/html/body/div[2]/div[1]/div/div[4]/div[7]/div[1]/form/div[7]/div/div/div[2]/input';
const Captcha3ImageXpath = '/html/body/div[2]/div[1]/div/div[4]/div[10]/div/div[2]/form/div[10]/div/div/div[1]/img';
const Captcha3FieldXpath = '/html/body/div[2]/div[1]/div/div[4]/div[10]/div/div[2]/form/div[10]/div/div/div[2]/input';
const Button1Xpath = '//*[@id="tab_section_calc"]/div[6]/div[1]/form/div[2]/button';
const Button2Xpath = '//*[@id="tab_section_calc"]/div[7]/div[2]/div[1]/div[2]/button';
const Button3Xpath = '/html/body/div[2]/div[1]/div/div[4]/div[10]/div/div[2]/form/div[11]/div/button';
const ButtonBuyOnlineXpath = '/html/body/div[2]/div[1]/div/div[4]/div[7]/div[2]/div[2]/div[2]/div/div/div/button';

//ШАГ 3
const OwnerSurnameFieldXpath = '//*[@id="ownerExtendedLastName"]';
const OwnerFirstNameFieldXpath = '//*[@id="ownerExtendedFirstName"]';
const OwnerSecondNameFieldXpath = '//*[@id="ownerExtendedMiddleName"]';
const OwnerBirthdayFieldXpath = '//*[@id="ownerExtendedBirthDate"]';
const OwnerDocIssueDateFieldXpath = '//*[@id="ownerExtendedDocumentIssueDate"]';
const OwnerDocSeriesFieldXpath = '//*[@id="ownerExtendedDocumentSeries"]';
const OwnerDocNumberFieldXpath = '//*[@id="ownerExtendedDocumentNumber"]';
const OwnerStreetFieldXpath = '//*[@id="ownerExtendedAddressStreet"]';
const OwnerHouseFieldXpath = '/html/body/div[2]/div[1]/div/div[4]/div[10]/div/div[2]/form/div[1]/div[2]/div[9]/input';
const OwnerCorpusFieldXpath = '//*[@id="ownerExtendedAddressStructure"]';
const OwnerApartmentsFieldXpath = '//*[@id="ownerExtendedAddressApartment"]';

const InsurerSurnameFieldName = 'insurerLastName';
const InsurerFirstNameFieldName = 'insurerFirstName';
const InsurerSecondNameFieldName = 'insurerMiddleName';
const InsurerBirthdayFieldXpath = '//*[@id="insurerExtendedBirthDate"]';
const InsurerDocIssueDateFieldXpath = '//*[@id="insurerExtendedDocumentIssueDate"]';
const InsurerDocSeriesFieldXpath = '//*[@id="insurerExtendedDocumentSeries"]';
const InsurerDocNumberFieldXpath = '//*[@id="insurerExtendedDocumentNumber"]';
const InsurerCityFieldXpath = '//*[@id="tab_section_calc"]/div[10]/div/div[2]/form/div[2]/div[2]/div[7]/input[1]';
const InsurerStreetFieldXpath = '//*[@id="insurerExtendedAddressStreet"]';
const InsurerHouseFieldXpath = '//*[@id="insurerExtendedAddressHouse"]';
const InsurerCorpusFieldXpath = '//*[@id="insurerExtendedAddressStructure"]';
const InsurerApartmentsFieldXpath = '//*[@id="insurerExtendedAddressApartment"]';

const TSDocFieldName = 'tsPtcMakeModelNum';
const TSDocTypeFieldName = 'tsDocumentType';
const TSDocSeriesNumberFieldName = 'tsDocumentSeriesNumber';
const TSDocIssueDateFieldName = 'tsDocumentDate';
const TSGosNumberFieldName = 'tsLicensePlate';
const TSBodyNumberFieldName = 'tsBody';
const TSChassisFieldName = 'tsChassis';
const TSVINFieldName = 'tsVin';

const TSTOTypeFieldName = 'tsTOType';
const TSTOSeriesNumberFieldName = 'tsTOSerialNumber';
const TSTODateFieldName = 'tsTODate';
const TSTOFinishDateFieldName = 'tsTODateNext';

const TSIsSeasonalXpath = '//*[@id="tab_section_calc"]/div[10]/div/div[2]/form/div[6]/div/div/div[1]/label[1]/input';
const TSIsNotSeasonalXpath = '//*[@id="tab_section_calc"]/div[10]/div/div[2]/form/div[6]/div/div/div[1]/label[2]/input';
const TSIsRentXpath = '//*[@id="tab_section_calc"]/div[10]/div/div[2]/form/div[6]/div/div/div[2]/label[1]/input';
const TSIsNotRentXpath = '//*[@id="tab_section_calc"]/div[10]/div/div[2]/form/div[6]/div/div/div[2]/label[2]/input';

async function Authorization (driver, ReqBody) {
  const { login } = ReqBody;
  await driver.get(authURL);
  await setValue(AuthLoginName, login, driver);
  await setValue(AuthPasswordName, userPassword, driver);
  await driver.sleep(200);
  await clickValue(driver, AuthButtonName);
}

async function PolicyCalculation (driver, ReqBody) {
  await driver.wait(CheckLocatedElement(driver, By.xpath(GoToCalculationPageXpath)), 10000);
  await driver.get('https://direct.sogaz.ru/products/automobile/osago/calc/');
  await InputAndDataListComparator(driver, ReqBody);
}

async function InputAndDataListComparator (driver, ReqBody) {
  const {
    city, region, gender,
    carCategory, carMark, carModel, carPower, carReleaseYear, carUnladenMass, carMaxMass, carPassagerCount,
    usingTarget, usingPeriod, goingToRegPlace, usingWithTrailer,
    policyStartDate, infinityDriversCount, drivers
  } = ReqBody;
  console.log('City: ', city, ' Region: ', region);
  console.log('Mark: ', carMark, ' Model: ', carModel);
  await driver.wait(CheckLocatedElement(driver, By.xpath(LocationCityFieldXpath)), 10000);

  // Город - г, Деревня - д, Поселок - п, Село - с, Хутор - х, Аул - аул
  // Область - обл, Край - край, Республика - Респ, Автономный Округ (АО) - АО

  function NewRegion (region) {
    let cutRegion;
    if (region.includes('обл')) {
      cutRegion = region.slice(3).trim();
    } else if (region.includes('край')) {
      cutRegion = region.slice(4).trim();
    } else if (region.includes('Респ')) {
      cutRegion = region.slice(4).trim();
    } else if (region.includes('АО')) {
      cutRegion = region.slice(2).trim();
    } else {
      console.log('NEW VALUE: ' + region);
    }
    console.log('cutRegion: ' + cutRegion);
  }

  function NewCity (city) {
    let cutCity;
    if (city.includes('аул')) {
      console.log('SECOND');
      cutCity = city.slice(3).trim();
      if (cutCity.includes('[')) {
        let splitCity = cutCity.split(' ');
        cutCity = splitCity[0];
      }
    } else {
      if (city.includes('г') || city.includes('с', 0) || city.includes('п', 0) || city.includes('д', 0) || city.includes('х', 0)) {
        console.log('FIRST');
        cutCity = city.slice(1).trim();
        if (cutCity.includes('[')) {
          let splitCity = cutCity.split(' ');
          cutCity = splitCity[0];
        }
      } else {
        console.log('NEW VALUE: ' + city);
      }
    }
    console.log('cutCity: ' + cutCity);
  }

  if (city.includes('г', 0) || city.includes('с', 0) || city.includes('п', 0) || city.includes('д', 0) || city.includes('х', 0) || city.includes('а', 0)) {
    cutCity = await city.slice(1).trim();
  }

  if (region.includes('обл', 0) || region.includes('край', 0) || region.includes('Респ', 0) || region.includes('АО', 0)) {
    cutRegion = await region.slice(3).trim();
  }

  // await driver.wait(CheckLocatedElement(driver, By.xpath('/html/body/div[9]/div/a')), 5000);
  // await driver.wait(CheckLocatedElement(driver, By.className('fancybox-skin')), 5000).then(async function () {
  //   console.log('BOX SEARCHED');
  //   await driver.findElement(By.xpath('/html/body/div[9]/div/a')).click();
  // });

  await driver.wait(webdriver.until.elementLocated(By.xpath(LocationCityFieldXpath)), 10000).then(element => {
    return driver.wait(webdriver.until.elementIsEnabled(element), 10000).then(async () => {
      await console.log('ELEMENT IS ENABLED');
      await driver.findElement(By.xpath(LocationCityFieldXpath)).sendKeys(cutCity, ' ', cutRegion);
    });
  });

  await driver.wait(CheckLocatedElement(driver, By.xpath('/html/body/div[9]/div[1]')), 5000);
  for (let i = 1; i < 6; i++) {
    let fieldTxt = await driver.findElement(By.xpath(`/html/body/div[9]/div[${i}]`)).getText();
    if (fieldTxt.includes(cutCity + ' ') && fieldTxt.includes(cutRegion + ' ')) {
      await driver.findElement(By.xpath(`/html/body/div[9]/div[${i}]`)).click();
      i = 6;
    }
  }

  await driver.wait(CheckLocatedElement(driver, By.xpath(CategoryAXpath)), 5000);
  for (let i = 2; i < 6; i++) {
    let fieldTxt = await driver.findElement(By.xpath(`//*[@id="tab_section_calc"]/div[6]/div[1]/form/div[2]/div[1]/div[1]/div[2]/div/div/select/option[${i}]`)).getText();
    fieldTxt.trim();
    if (fieldTxt.includes(carCategory)) {
      await driver.findElement(By.xpath(`//*[@id="tab_section_calc"]/div[6]/div[1]/form/div[2]/div[1]/div[1]/div[2]/div/div/select/option[${i}]`)).click();
      i = 6;
    }
  }

  await driver.sleep(200);
  await driver.wait(CheckLocatedElement(driver, By.className('b-ts-select-brand')), 10000);
  if (carMark.toLowerCase() === 'bmw') {
    if (carModel === '125') {
      await driver.findElement(By.xpath(`//a[contains(@class, \'b-ts-select-brand\')][text()='${carMark.toLowerCase()}']`)).click();
      await driver.wait(CheckLocatedElement(driver, By.className('b-ts-select-model')), 5000);
      await driver.findElement(By.xpath(`//a[contains(@class, \'b-ts-select-model\')][text()='1er']`)).click();
    }
  } else {
    await driver.findElement(By.xpath(`//a[contains(@class, \'b-ts-select-brand\')][text()='${carMark.toLowerCase()}']`)).click();
    await driver.wait(CheckLocatedElement(driver, By.className('b-ts-select-model')), 5000);
    await driver.findElement(By.xpath(`//a[contains(@class, \'b-ts-select-model\')][text()='${carModel.toLowerCase()}']`)).click();
  }

  await driver.findElement(By.className('b-ts-select-configuration')).click();

  await driver.findElement(By.name(TSPowerFieldName)).sendKeys(carPower);
  await driver.findElement(By.name(TSReleaseYearFieldName)).sendKeys(carReleaseYear);
  await driver.findElement(By.css('#calculatorTsUse > option:nth-child(2)')).click();
// await driver.wait(CheckLocatedElement(driver, By.name(TSUsingTargetPersonalFieldXpath)), 5000);
// await driver.findElement(By.name(TSUsingTargetPersonalFieldXpath)).click();

  if (carCategory === 'С, СЕ') {
    await driver.findElement(By.name(TSMaxMassFieldName)).sendKeys(carMaxMass);
    await driver.findElement(By.name(TSUnladenMassFieldName)).sendKeys(carUnladenMass);
  } else if (carCategory === 'D, DE') {
    await driver.findElement(By.name(TSPassagerCountFieldName)).sendKeys(carPassagerCount);
  }

  for (let i = 2; i < 10; i++) {
    let fieldTxt = await driver.findElement(By.xpath(`//*[@id="calculatorInsurancePeriodRussia"]/option[${i}]`)).getText();
    if (fieldTxt.includes(usingPeriod)) {
      await driver.findElement(By.xpath(`//*[@id="calculatorInsurancePeriodRussia"]/option[${i}]`)).click();
      i = 10;
    }
  }

  await (goingToRegPlace) ? driver.findElement(By.xpath(TSGoingToRegPlaceXpath)).click() : driver.findElement(By.xpath(TSNotGoingToRegPlaceXpath)).click();
  await (usingWithTrailer) ? driver.findElement(By.xpath(TSUsingTrailerXpath)).click() : driver.findElement(By.xpath(TSNotUsingTrailerXpath)).click();
  if (!infinityDriversCount) {

    for (let i = 1; i < 6; i++) {
      let fieldTxt = await driver.findElement(By.xpath(`//*[@id="tab_section_calc"]/div[6]/div[1]/form/div[2]/div[3]/div/div/span/label[${i}]`)).getText();
      fieldTxt.trim();
      if (fieldTxt.includes(drivers.length)) {
        await driver.findElement(By.xpath(`//*[@id="tab_section_calc"]/div[6]/div[1]/form/div[2]/div[3]/div/div/span/label[${i}]`)).click();
        i = 10;
      }
    }
  } else {
    await driver.findElement(By.xpath(DriverCountIsInfinityXpath)).click();
  }

  await driver.findElement(By.xpath(StartInsuranceDateFieldXpath)).sendKeys(policyStartDate);

  await CaptchaChecker(driver, Captcha1FieldXpath, Captcha1ImageXpath, Button1Xpath);

  await driver.wait(CheckLocatedElement(driver, By.className('fancybox-skin')), 5000);
  await driver.wait(webdriver.until.elementLocated(By.xpath('/html/body/div[17]/div/div/div/div/div/div/div/button')), 10000).then(element => {
    return driver.wait(webdriver.until.elementIsEnabled(element), 10000).then(async () => {
      await console.log('ELEMENT IS ENABLED');
      await driver.findElement(By.xpath('/html/body/div[17]/div/div/div/div/div/div/div/button')).click();
    });
  });
  await driver.sleep(1000);

  if (!infinityDriversCount) {
    await drivers.map(async (item, index) => {
      await console.log('ITEM INDEX: ', index);
      const { surname, firstName, secondName, birthday, gender, licenseCategory, licenseNumber, licenseDateOfIssue, firstLicenseDateOfIssue } = item;
      await driver.wait(CheckLocatedElement(driver, By.name(`driver${index}LastName`)), 5000);
      await driver.findElement(By.name(`driver${index}LastName`)).sendKeys(surname);
      await driver.findElement(By.name(`driver${index}FirstName`)).sendKeys(firstName);
      await driver.findElement(By.name(`driver${index}MiddleName`)).sendKeys(secondName);
      await driver.findElement(By.name(`driver${index}BirthDate`)).sendKeys(birthday);
      if (gender === 'male') {
        await driver.wait(CheckLocatedElement(driver, By.xpath(`//*[@name="driver${index}Sex"][@value= 'm']`)), 5000);
        await driver.findElement(By.xpath(`//*[@name="driver${index}Sex"][@value= 'm']`)).click();
      } else {
        await driver.wait(CheckLocatedElement(driver, By.xpath(`//*[@name="driver${index}Sex"][@value= 'f']`)), 5000);
        await driver.findElement(By.xpath(`//*[@name="driver${index}Sex"][@value= 'f']`)).click();
      }
      await driver.sleep(1000);
      await driver.wait(CheckLocatedElement(driver, By.xpath(`//*[@name="driver${index}LicenceCategory[]"][@value='${licenseCategory.toUpperCase()}']`)), 5000);
      await driver.findElement(By.xpath(`//*[@name="driver${index}LicenceCategory[]"][@value='${licenseCategory.toUpperCase()}']`)).click();
      await driver.findElement(By.name(`driver${index}LicenceNum`)).sendKeys(licenseNumber);
      await driver.findElement(By.name(`driver${index}LicenceDate`)).sendKeys(licenseDateOfIssue);
      await driver.findElement(By.name(`driver${index}LicenceFirstDate`)).sendKeys(firstLicenseDateOfIssue);
    });
  }

  await CaptchaChecker(driver, Captcha2FieldXpath, Captcha2ImageXpath, Button2Xpath);

  await driver.sleep(3000);

  await driver.wait(webdriver.until.elementLocated(By.className('btn mg-tp_xlg eosago-calculatorkbm-form-buy-online')), 10000).then(element => {
    return driver.wait(webdriver.until.elementIsEnabled(element), 10000).then(async () => {
      await console.log('ELEMENT IS ENABLED');
      await driver.findElement(By.className('btn mg-tp_xlg eosago-calculatorkbm-form-buy-online')).click();
    });

  });

  const { ownerSurname, ownerFirstName, ownerSecondName, ownerBirthday, ownerGender, ownerDocType, ownerDocSeries, ownerDocNumber, ownerDocIssueDate, ownerStreet, ownerHouse, } = ReqBody;

  await driver.sleep(1000);
  await driver.wait(CheckLocatedElement(driver, By.xpath(OwnerSurnameFieldXpath)), 5000);
  await driver.findElement(By.xpath(OwnerSurnameFieldXpath)).sendKeys(ownerSurname);
  await driver.findElement(By.xpath(OwnerFirstNameFieldXpath)).sendKeys(ownerFirstName);
  await driver.findElement(By.xpath(OwnerSecondNameFieldXpath)).sendKeys(ownerSecondName);
  await driver.findElement(By.xpath(OwnerBirthdayFieldXpath)).sendKeys(ownerBirthday);
  if (ownerGender === 'male') {
    await driver.wait(CheckLocatedElement(driver, By.xpath('//*[@name="ownerSexDisabled"][@value="m"]')), 5000);
    await driver.findElement(By.xpath('//*[@name="ownerSexDisabled"][@value="m"]')).click();
  } else {
    await driver.wait(CheckLocatedElement(driver, By.xpath('//*[@name="ownerSexDisabled"][@value="f"]')), 5000);
    await driver.findElement(By.xpath('//*[@name="ownerSexDisabled"][@value="f"]')).click();
  }

  await driver.findElement(By.xpath(OwnerDocSeriesFieldXpath)).click();
  await driver.sleep(500);
  await driver.findElement(By.xpath(OwnerDocSeriesFieldXpath)).sendKeys(ownerDocSeries);
  await driver.sleep(500);
  await driver.findElement(By.xpath(OwnerDocNumberFieldXpath)).sendKeys(ownerDocNumber);
  await driver.findElement(By.xpath(OwnerDocIssueDateFieldXpath)).sendKeys(ownerDocIssueDate);
  await driver.findElement(By.xpath(OwnerStreetFieldXpath)).sendKeys(ownerStreet);
  await driver.sleep(500);
  await driver.findElement(By.xpath(OwnerHouseFieldXpath)).sendKeys(ownerHouse);
  await driver.wait(CheckLocatedElement(driver, By.xpath('/html/body/div[15]/div[1]')), 5000);
  await driver.sleep(1000);
  for (let i = 1; i < 6; i++) {
    let fieldTxt = await driver.findElement(By.xpath(`/html/body/div[15]/div[${i}]`)).getText();
    if (fieldTxt.length === ownerHouse.length && fieldTxt.includes(ownerHouse)) {
      await driver.findElement(By.xpath(`/html/body/div[15]/div[${i}]`)).click();
      i = 6;
    }

    // let fieldTxt = await (ownerHouse.length === 1)
    //   ? driver.findElement(By.xpath(`//*[@class="autocomplete-suggestion"][text()='${ownerHouse}']`)).getText()
    //   : driver.findElement(By.xpath(`//*[@class="autocomplete-suggestion"][text()='${ownerHouse.slice(0, -1)}'][text()='${ownerHouse.slice(1)}']`)).getText();
    // if (fieldTxt.length === ownerHouse.length && fieldTxt.includes(ownerHouse)) {
    //   console.log('OWNER HOUSE: ', fieldTxt);
    //   if (ownerHouse.length === 1)
    //     await driver.findElement(By.xpath(`//*[@class="autocomplete-suggestion"][text()='${ownerHouse}']`)).click();
    //   else
    //     await driver.findElement(By.xpath(`//*[@class="autocomplete-suggestion"][text()='${ownerHouse.slice(0, -1)}'][text()='${ownerHouse.slice(1)}']`)).click();
    //   i = 6;
    // }
    // //*[@class="autocomplete-suggestion"]//*[text()='3'][text()='Б']
  }

  const { insurerBirthday, insurer, insurerDocType, insurerDocSeries, insurerDocNumber, insurerDocIssueDate, insurerCity, insurerRegion, insurerStreet, insurerHouse } = ReqBody;

  await driver.wait(CheckLocatedElement(driver, By.xpath(InsurerBirthdayFieldXpath)), 5000);
  await driver.findElement(By.xpath(InsurerBirthdayFieldXpath)).sendKeys(insurerBirthday);
  if (insurer === 'male') {
    await driver.wait(CheckLocatedElement(driver, By.xpath('//*[@name="insurerSex"][@value="m"]')), 5000);
    await driver.findElement(By.xpath('//*[@name="insurerSex"][@value="m"]')).click();
  } else {
    await driver.wait(CheckLocatedElement(driver, By.xpath('//*[@name="insurerSex"][@value="f"]')), 5000);
    await driver.findElement(By.xpath('//*[@name="insurerSex"][@value="f"]')).click();
  }
  await driver.findElement(By.xpath(InsurerDocIssueDateFieldXpath)).sendKeys(insurerDocIssueDate);
  await driver.sleep(500);
  await driver.findElement(By.xpath(InsurerDocSeriesFieldXpath)).click();
  await driver.sleep(500);
  await driver.findElement(By.xpath(InsurerDocSeriesFieldXpath)).sendKeys(insurerDocSeries);
  await driver.sleep(500);
  await driver.findElement(By.xpath(InsurerDocNumberFieldXpath)).sendKeys(insurerDocNumber);

  if (insurerCity.includes('г', 0) || insurerCity.includes('с', 0) || insurerCity.includes('п', 0) || insurerCity.includes('д', 0) || insurerCity.includes('х', 0) || insurerCity.includes('а', 0)) {
    cutCity = await insurerCity.slice(1).trim();
  }
  if (insurerRegion.includes('обл', 0)) {
    cutRegion = await insurerRegion.slice(3).trim();
  }

  await driver.findElement(By.xpath(InsurerCityFieldXpath)).sendKeys(cutCity, ' ', cutRegion);
  await driver.wait(CheckLocatedElement(driver, By.xpath('/html/body/div[10]/div[1]')), 5000);

  for (let i = 1; i < 6; i++) {
    let fieldTxt = await driver.findElement(By.xpath(`/html/body/div[10]/div[${i}]`)).getText();
    if (fieldTxt.includes(cutCity + ' ') && fieldTxt.includes(cutRegion + ' ')) {
      await driver.findElement(By.xpath(`/html/body/div[10]/div[${i}]`)).click();
      i = 6;
    }
  }
  await driver.findElement(By.xpath(InsurerStreetFieldXpath)).sendKeys(insurerStreet);
  await driver.sleep(500);
  await driver.findElement(By.xpath(InsurerHouseFieldXpath)).sendKeys(insurerHouse);
  await driver.sleep(500);
  await driver.wait(CheckLocatedElement(driver, By.xpath('/html/body/div[13]/div[1]')), 5000);
  for (let i = 1; i < 5; i++) {
    let fieldTxt = await driver.findElement(By.xpath(`/html/body/div[13]/div[${i}]`)).getText();
    if (fieldTxt.length === insurerHouse.length && fieldTxt.includes(insurerHouse)) {
      await driver.findElement(By.xpath(`/html/body/div[13]/div[${i}]`)).click();
      i = 6;
    }
  }

  const { tsDocType, tsDocSeriesNumber, tsIssueDate, tsGosNumber, tsBodyNumber, tsBodyNumberIsAbsent, tsChassisNumber, tsChassisNumberIsAbsent, tsVINNumber, tsVINIsAbsent } = ReqBody;
  await driver.findElement(By.name(TSDocFieldName)).sendKeys(carMark, ' ', carModel);
  await driver.findElement(By.name(TSDocTypeFieldName)).sendKeys(tsDocType);
  await driver.findElement(By.name(TSDocSeriesNumberFieldName)).sendKeys(tsDocSeriesNumber);
  await driver.findElement(By.xpath('//*[@id="tsLicensePlate"]')).sendKeys(tsIssueDate);
  await driver.wait(CheckLocatedElement(driver, By.name(TSGosNumberFieldName)), 5000);
  await driver.findElement(By.xpath('//*[@id="tsLicensePlate"]')).sendKeys(tsGosNumber);
  await (tsBodyNumberIsAbsent)
    ? driver.findElement(By.xpath('//*[@id="tab_section_calc"]/div[10]/div/div[2]/form/div[4]/div[2]/div[20]/label[1]/input')).click()
    : driver.findElement(By.xpath('//*[@id="tsBody"]')).sendKeys(tsBodyNumber);
  await (tsChassisNumberIsAbsent)
    ? driver.findElement(By.xpath('//*[@id="tab_section_calc"]/div[10]/div/div[2]/form/div[4]/div[2]/div[21]/label[1]/input')).click()
    : driver.findElement(By.xpath('//*[@id="tsChassis"]')).sendKeys(tsChassisNumber);
  await (tsVINIsAbsent)
    ? driver.findElement(By.xpath('//*[@id="tab_section_calc"]/div[10]/div/div[2]/form/div[4]/div[2]/div[22]/label[1]/input')).click()
    : driver.findElement(By.xpath('//*[@id="tsVin"]')).sendKeys(tsVINNumber);

  const { toType, toSeriesNumber, toIssueDate, toFinishDate } = ReqBody;

  for (let i = 2; i < 4; i++) {
    for (let i = 1; i < 5; i++) {
      let fieldTxt = await driver.findElement(By.xpath(`//*[@id="tsTOType"]/option[${i}]`)).getText();
      if (fieldTxt.includes(toType)) {
        await driver.findElement(By.xpath(`//*[@id="tsTOType"]/option[${i}]`)).click();
        i = 6;
      }
    }
  }
  await driver.findElement(By.name(TSTOSeriesNumberFieldName)).sendKeys(toSeriesNumber);
  await driver.findElement(By.name(TSTODateFieldName)).sendKeys(toIssueDate);
  await driver.findElement(By.name(TSTOFinishDateFieldName)).sendKeys(toFinishDate);

  const { isSeasonalTS, isRentTS } = ReqBody;

  await (isSeasonalTS) ? driver.findElement(By.xpath(TSIsSeasonalXpath)).click() : driver.findElement(By.xpath(TSIsNotSeasonalXpath)).click();
  await (isRentTS) ? driver.findElement(By.xpath(TSIsRentXpath)).click() : driver.findElement(By.xpath(TSIsNotRentXpath)).click();

  await CaptchaChecker(driver, Captcha3FieldXpath, Captcha3ImageXpath, Button3Xpath);
}

async function CaptchaChecker (driver, captchaFieldXpath, captchaImageXpath, nextButtonXpath) {
  CaptchaText = '';
  await TakeScreenShot(driver, CaptchaText, captchaImageXpath);
  await driver.wait(async function () {
    let num = 0;
    do {
      await driver.sleep(800);
      if (CaptchaText !== '') {
        return num = 1;
      }
    } while (num < 1);
  }, 5000);
  await console.log('CAPTCHA TXT: ', CaptchaText);
  await driver.findElement(By.xpath(captchaFieldXpath)).clear().then(async function () {
    await driver.findElement(By.xpath(captchaFieldXpath)).sendKeys(CaptchaText);
    await driver.findElement(By.xpath(nextButtonXpath)).click();
  });

  try {
    await driver.sleep(1500);
    await driver.findElement(By.id('captchaWord-error')).isDisplayed().then(async function (state) {
      console.log(state);
      if (state) {
        await CaptchaChecker(driver, captchaFieldXpath, captchaImageXpath, nextButtonXpath);
        return;
      }
    });
  } catch (e) {
  }
}

/**
 * @return {string}
 */
async function TakeScreenShot (driver, text, srcXpath, imgXpath = '/html/body/img') {
  let srcToImage = '';
  await driver.sleep(500);
  srcToImage = await driver.findElement(By.xpath(srcXpath)).getAttribute('src');
  await console.log('SRC: ', srcToImage);
  const driver2 = createBrowser('chrome', true);
  await driver2.get(srcToImage);
  let img = await driver2.findElement(By.xpath(imgXpath));
  img.takeScreenshot().then(async function (image, err) {
    await require('fs').writeFile('captcha.png', image, 'base64', async function () {
      if (!err) {
        solver.decode(image.toString('base64'), (err, result, invalid) => {
          if (err) {
            console.log('Solver error: ', err);
            TakeScreenShot(driver, text, srcXpath);
            return;
          }
          console.log('SOLVER DECODE: ', result.text.toUpperCase());
          CaptchaText = result.text.toUpperCase();
        });
        await console.log('CAPTCHA SENDED TO DECODING');
        await driver2.quit();
      } else await console.log('ERROR: ', err);
    });
  });
  return CaptchaText;
}

app.route('/reg-user-with-owner-data-and-get-policy').post(async function (request, response) {
  console.log('Request Body: ', request.body);
  if (request.body !== undefined) {
    const { ownerSurname, ownerFirstName, ownerSecondName, ownerBirthday, ownerDocSeries, ownerDocNumber, ownerPhone } = request.body;
    userSurname = ownerSurname;
    userFirstName = ownerFirstName;
    userSecondName = ownerSecondName;
    userBirthday = ownerBirthday;
    userDocSeries = ownerDocSeries;
    userDocNumber = ownerDocNumber;
    userPhone = ownerPhone;

    await CreatePolicyQueue.add(async () => {
      const driver = await createBrowser('chrome');
      let login = await AccountRegistrationAndPolicyFilling(driver, false, request.body);
      await response.json(login);
    });
    return;
  }
  return response.status(404).send('Request body is undefined');
});

app.route('/reg-with-data/lk').post(async function (request, response) {
  console.log('Request Body: ', request.body);
  if (request.body !== undefined) {
    const { surname, firstName, patronymic, birthday, docSeries, docNumber, phone } = request.body;
    userSurname = surname;
    userFirstName = firstName;
    userSecondName = patronymic;
    userBirthday = birthday;
    userDocSeries = docSeries;
    userDocNumber = docNumber;
    userPhone = phone;

    await RegLKQueue.add(async () => {
      let login = await SingleAccountRegistration();
      await response.json(login);
    });
    return;
  }
  return response.status(404).send('Request body is undefined');
});

app.route('/reg/lk').post(async function (request, response) {
  console.log('Account registration is started');
  await RegLKQueue.add(async () => {
    const acc = await SingleAccountRegistration();
    if (acc !== null || acc !== '') {
      await response.json(acc);
      return;
    }
  });
  return response.status(404).send('Can not create account');
});

app.route('/reg-max-count/lk').post(async function (request, response) {
  console.log('Max account registration is started');
  await RegMaxLKQueue.add(async () => {
    const regAcc = await AccountRegistration();
    let accArr = [];
    if (regAcc) {
      await accArr.push(regAcc);
      await response.json(regAcc);
      console.log('Account has been created: ', regAcc);
      await AccountCounter();
      if (accountsInDB === 25) {
        await response.send('That all registered accounts:');
        await response.json(accArr);
      }
      return;
    }
  });
  return response.status(404).send('Can not create account');
});

app.route('/get/lk').get(async function (request, response) {
  await GetLKQueue.add(async () => {
    await GetLoginFromDB(request, response);
  });
});

app.route('/show-registered/lk').post(async function (request, response) {
  await ShowRegLKQueue.add(async () => {
    let regACCs = ShowRegisteredAccounts(request, response);
    if (regACCs !== undefined && regACCs !== []) {
      console.log('That all registered accounts: ', regACCs);
      response.json(regACCs);
      return;
    }
  });
  return response.status(404).send('It was not possible to show all  created accounts');
});

/**
 * @return {string}
 */
async function AccountRegistrationAndPolicyFilling (driver, quitBrowser, ReqBody) {
  await GetEmail(driver).then(function () {
    return GoToRegistration().then(function () {
      return GoToEmail(driver, quitBrowser).then(function () {
        console.log('Account is registered, email: ', userEmail);
        return PolicyCalculation(driver, ReqBody).then(async function () {
        });
      });
    });
  });
  return userEmail;
}

/**
 * @return {string}
 */
async function SingleAccountRegistration () {
  const driver = await createBrowser('chrome');
  await GetEmail(driver).then(function () {
    return GoToRegistration().then(function () {
      return GoToEmail(driver).then(function () {
        console.log('Account is registered, email: ', userEmail);
      });
    });
  });
  return userEmail;
}

async function AccountRegistration (counter = defaultACCsCount - accountsInDB) {
  let value = counter;
  console.log('Will be registered accounts: ', value);
  const driver = await createBrowser('chrome');
  await GetEmail(driver).then(function () {
    return GoToRegistration().then(function () {
      return GoToEmail(driver).then(function () {
        if (value !== 0) {
          value--;
          SaveLoginInDB(userEmail);
          AccountRegistration(value);
          console.log('Account is registered, email: ', userEmail);
          return;
        }
        ShowAllAccounts();
        process.exit();
      });
    });
  });
}

app.listen(port, async function () {
  console.log('SOGAZ Bot started on ' + port + ' port');
  await solver.setApiKey('a97c45924a32fa052f3b6ea056de20b1');
});
